class Route {
    constructor(id, startTime) {
        this.id = id;
        this.startTime = startTime;
        this.points = [];
    }

    getId() {
        return this.id;
    }

    getStartTime() {
        return this.startTime;
    }

    getPoints() {
        return this.points;
    }

    addPoint(point) {
        this.points.push(point);
    }

}


class Point {
    constructor(id, lat, lng, alt, time) {
        this.id = id;
        this.lat = lat;
        this.lng = lng;
        this.alt = alt;
        this.time = time;
    }

    getLat() {
        return this.lat;
    }

    getLng() {
        return this.lng;
    }

    equals(lat, lng) {
        if (lat === this.getLat() && lng === this.getLng()) {
            return true;
        } else {
            return false;
        }
    }
}

var route = {};
var oldPoint = null;


function insertRoute() {
    var dataString = "insert=";

    $.ajax({
        type: "POST",
        url: "../php/insert-route.php",
        data: dataString,
        crossDomain: true,
        cache: false,
        beforeSend: function () {
        },
        success: function (data) {
            getRouteInfo();
        },
        error: function () {
            alert("error on route");
        }
    });

}

function insertPoint(lat, lng, alt) {
    console.log("in insert ajax");

    //Get rid of consecutive dupliates to save space
    if (oldPoint !== null) {
        if (oldPoint.equals(lat, lng)) {
            console.log("equals triggered");
            return;
        }
    }
    oldPoint = new Point(null, lat, lng, alt, null);

    //If alts not avaiable replace with a default
    alt = alt === null ? -1000 : alt;

    var dataString = "route_id=" + route.getId() + "&lat=" + lat + "&lng=" + lng + "&alt=" + alt + "&insert=x";
    $.ajax({
        type: "POST",
        url: "../php/insert-point.php",
        data: dataString,
        crossDomain: true,
        cache: false,
        beforeSend: function () {
            console.log(dataString);
        },
        success: function (data) {
            //alert("inserted");
        },
        error: function (data) {
        }
    });

}

function updateRouteName(name) {

    return new Promise(function (fulfill, reject) {

        console.log("in update name");
        var dataString = "route_name=" + name + "&route_id=" + route.getId() + "&insert=x";
        $.ajax({
            type: "POST",
            url: "../php/update-route-name.php",
            data: dataString,
            crossDomain: true,
            cache: false,
            beforeSend: function () {
                console.log(dataString);
            },
            success: function (data) {
                fulfill();
            },
            error: function (data) {
                reject();
            }
        });

    });

}


function getRouteInfo() {
    var url = "../php/read-route.php";
    $.getJSON(url, function (result) {
        var i = result.length - 1;

        var route_id = result[i].ROUTE_ID;
        var start_time = result[i].START_TIME;
        route = new Route(route_id, start_time);
        console.log("new route " + route_id);
    });

}

function getPoints() {


    return new Promise(function (fulfill, reject) {

        var url = "../php/read-point.php";
        console.log(route.getId());
        var dataString = "route_id=" + route.getId() + "&insert=";

        $.ajax({
            type: "POST",
            url: url,
            data: dataString,
            crossDomain: true,
            cache: false,
            beforeSend: function () {
            },
            success: function (data) {
                var obj = JSON.parse(data);
                console.log(obj);
                for (var key in obj) {
                    if (obj.hasOwnProperty(key)) {
                        var id = obj[key].POINT_ID;
                        var lat = obj[key].LAT;
                        var lng = obj[key].LNG;
                        var alt = obj[key].ALT;
                        var time = obj[key].TIME;

                        route.addPoint(new Point(id, lat, lng, alt, time));

                    }
                }

                fulfill();
            },
            error: function (data) {
                alert("error on read");
                reject();
            }

        });
    });

}


function readRouteTime(callback) {

    var url = "../php/read-route-time.php";
    var route_id = route.getId();

    var dataString = "route_id=" + route_id + "&insert=";

    $.ajax({
        type: "POST",
        url: url,
        data: dataString,
        crossDomain: true,
        cache: false,
        beforeSend: function () {
        },
        success: function (data) {
            callback(data);
        },
        error: function (data) {
            alert("error on read");
        }

    });

}

function updateRouteSpeedTime(data) {
    var route_time;
    var avg_speed;
    var route_distance;
    var total_distance;

    var obj = JSON.parse(data);

    route_time = obj[0]["route_time"];
    route_distance = routeDistance();
    total_distance = route_distance.slice(-1)[0];
    avg_speed = routeSpeed(total_distance, route_time);

    var dataJson = {
        route_id: route.getId(),
        avg_speed: avg_speed,
        route_distance: route_distance,
        total_distance: total_distance,
        route_time: route_time,
        insert: " "
    };

    $.ajax({
        type: "POST",
        url: "../php/update-route-speed-distance-time.php",
        // The key needs to match your method's input parameter (case-sensitive).
        data: dataJson,
        dataType: "json",
        success: function (data) {
            console.log(data);
        },
        failure: function (errMsg) {
            alert(errMsg);
        }
    });


}

/**************************CALCULATIONS***************************/

// function routeDistance() {
//
//     var nPoints = route.getPoints().length;
//     console.log("nPoints: " + nPoints);
//     /*Sample rate*/
//     var sampleRate = 5;
//     var totalDistance = 0;
//     var distance = []
//     distance.push(0);
//
//     var points = route.getPoints();
//     var point1;
//     var point2;
//
//     for (i = 0; i < nPoints; i += sampleRate) {
//         console.log(i);
//         if (i + sampleRate < nPoints) {
//             point1 = points[i];
//             point2 = points[i + sampleRate];
//             totalDistance += haversine(point1.getLat() * 1, point1.getLng() * 1, point2.getLat() * 1, point2.getLng() * 1);
//             distance.push(totalDistance);
//         }
//     }
//
//     console.log("routeDistance:" + totalDistance);
//     return totalDistance;
// }

function routeDistance() {

    var nPoints = route.getPoints().length;
    console.log("nPoints: " + nPoints);
    /*Sample rate*/
    var sampleRate = 1;
    var totalDistance = 0;
    var distance = [];
    distance.push(0);

    var points = route.getPoints();
    var point1;
    var point2;

    for (i = 0; i < nPoints; i += sampleRate) {
        console.log(i);
        if (i + sampleRate < nPoints) {
            point1 = points[i];
            point2 = points[i + sampleRate];
            totalDistance += haversine(point1.getLat() * 1, point1.getLng() * 1, point2.getLat() * 1, point2.getLng() * 1);
            distance.push(totalDistance.toFixed(2));
        }
    }

    console.log("routeDistance:" + totalDistance);
    return distance;
}

function routeSpeed(distance, time) {
    console.log("routeSpeed:" + (distance / ((time / 60) / 60)).toFixed(2));
    return (distance * 1 / ((time * 1 / 60) / 60)).toFixed(1);
}

Number.prototype.toRadians = function () {
    return this * Math.PI / 180;
}

function haversine(latitude1, longitude1, latitude2, longitude2) {
    // R is the radius of the earth in kilometers
    var R = 6371;

    var deltaLatitude = (latitude2 - latitude1).toRadians();
    var deltaLongitude = (longitude2 - longitude1).toRadians();
    latitude1 = latitude1.toRadians();
    latitude2 = latitude2.toRadians();

    var a = Math.sin(deltaLatitude / 2) *
        Math.sin(deltaLatitude / 2) +
        Math.cos(latitude1) *
        Math.cos(latitude2) *
        Math.sin(deltaLongitude / 2) *
        Math.sin(deltaLongitude / 2);
    var c = 2 * Math.atan2(Math.sqrt(a),
        Math.sqrt(1 - a));
    var d = R * c;
    return d * 0.621371; //km to mph
}


/*****************************************************************/

