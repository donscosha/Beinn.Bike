<?php
/**
 * Created by IntelliJ IDEA.
 * User: scott
 * Date: 23/01/2018
 * Time: 13:53
 */
include "connect.php";


$stmt = $conn->prepare("INSERT INTO `POINT` (`POINT_ID`, `ROUTE_ID`, `LAT`, `LNG`,`ALT`, `TIME`, `POINT_DISTANCE`,`TERRAIN`) "
    . "VALUES (NULL,?,?,?,?, CURRENT_TIMESTAMP,0,'')");

if($stmt == false){
    echo $conn->error;
}
$err = $stmt->bind_param("iddd", $route_id, $lat, $lng,$alt);

if($err == false){
    echo $stmt->error;
}
if (isset($_POST['insert'])) {



    $route_id = $_POST['route_id'];
    $lat = $_POST['lat'];
    $lng = $_POST['lng'];
    $alt = $_POST['alt'];

    echo "  INSERT INTO `POINT` (`POINT_ID`, `ROUTE_ID`, `LAT`, `LNG`,`ALT`, `TIME`, `POINT_DISTANCE`,`TERRAIN`) "
        . "VALUES (NULL,$route_id,$lat,$lng,$alt, CURRENT_TIMESTAMP,0,NULL)";

    $err = $stmt->execute();

    if($err == false){
        echo $stmt->error;
    }

    $stmt->close();
    $conn->close();

}
