<?php
/**
 * Created by IntelliJ IDEA.
 * User: scott
 * Date: 22/01/2018
 * Time: 19:56
 */

include "connect.php";


$stmt = $conn->prepare("  INSERT INTO `ROUTE` (`ROUTE_ID`, `ROUTE_NAME`, `START_TIME`, `END_TIME`, `AVG_SPEED`, `DISTANCE`, `ROUTE_TIME`) "
    . "VALUES (NULL, NULL, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP,0,0,NULL)");

if (isset($_POST['insert'])) {

    $stmt->execute();

    $stmt->close();
    $conn->close();

}


