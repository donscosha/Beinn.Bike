class Route {
    constructor(id, startTime) {
        this.id = id;
        this.startTime = startTime;
        this.points = [];
    }

    getId() {
        return this.id;
    }

    getStartTime() {
        return this.startTime;
    }

    getPoints() {
        return this.points;
    }

    addPoint(point) {
        this.points.push(point);
    }

}


class Point {
    constructor(id, lat, lng, alt, time,distance,terrain) {
        this.id = id;
        this.lat = lat;
        this.lng = lng;
        this.alt = alt;
        this.time = time;
        this.distance = distance;
        this.terrain = terrain;
    }

    getId(){
        return this.id;
    }
    getLat() {
        return this.lat;
    }

    getLng() {
        return this.lng;
    }

    getAlt(){
        return this.alt;
    }
    getDistance(){
        return this.distance;
    }

    getTerrain(){
        return this.terrain;
    }

    equals(lat, lng) {
        if (lat === this.getLat() && lng === this.getLng()) {
            return true;
        } else {
            return false;
        }
    }
}

var route = {};
var oldPoint = null;


function insertRoute() {
    var dataString = "insert=";

    $.ajax({
        type: "POST",
        url: "../php/insert-route.php",
        data: dataString,
        crossDomain: true,
        cache: false,
        beforeSend: function () {
        },
        success: function (data) {
            getRouteInfo();
        },
        error: function () {
            alert("error on route");
        }
    });

}

function insertPoint(lat, lng, alt) {
    console.log("in insert ajax");

    //Get rid of consecutive dupliates to save space
    if(oldPoint !== null) {
        if (oldPoint.equals(lat, lng)) {
            console.log("equals triggered");
            return;
        }
    }
        oldPoint = new Point(null, lat, lng, alt, null);

    //If alts not avaiable replace with an default
    alt = alt === null ? -1000 : alt;

    var dataString = "route_id=" + route.getId() + "&lat=" + lat + "&lng=" + lng + "&alt=" + alt + "&insert=x";
    $.ajax({
        type: "POST",
        url: "../php/insert-point.php",
        data: dataString,
        crossDomain: true,
        cache: false,
        beforeSend: function () {
            console.log(dataString);
        },
        success: function (data) {
            //alert("inserted");
        },
        error: function (data) {
            alert("error on insert point");
        }
    });

}

function updateRouteName(name) {
    console.log("in update name");
    var dataString = "route_name=" + name + "&route_id=" + route.getId() + "&insert=x";
    $.ajax({
        type: "POST",
        url: "../php/update-route-name.php",
        data: dataString,
        crossDomain: true,
        cache: false,
        beforeSend: function () {
            console.log(dataString);
        },
        success: function (data) {
            //alert("inserted");
        },
        error: function (data) {
            alert("error on insert point");
        }
    });

}



function requestPoints(route_id,callback) {
    var url = "read-point.php";
    var dataString = "route_id=" + route_id + "&insert=";

    $.ajax({
        type: "POST",
        url: url,
        data: dataString,
        crossDomain: true,
        cache: false,
        beforeSend: function () {
        },
        success: function (data) {
            callback(data);
        },
        error: function (data) {
            alert("error on read");
        }

    });

}

function requestAccel(route_id,callback) {
    var url = "read-accel.php";
    var dataString = "route_id=" + route_id + "&insert=";

    $.ajax({
        type: "POST",
        url: url,
        data: dataString,
        crossDomain: true,
        cache: false,
        beforeSend: function () {
        },
        success: function (data) {
            callback(data);
        },
        error: function (data) {
            alert("error on read");
        }

    });

}


function requestRoute(route_id,callback) {
    var url = "read-route.php";
    var dataString = "route_id=" + route_id + "&insert=";

    $.ajax({
        type: "POST",
        url: url,
        data: dataString,
        crossDomain: true,
        cache: false,
        beforeSend: function () {
        },
        success: function (data) {
            // var obj = JSON.parse(data);
            // console.log(obj);
            callback(data);
        },
        error: function (data) {
            alert("error on route read");
        }

    });


}