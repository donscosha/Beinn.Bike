<?php
/**
 * Created by IntelliJ IDEA.
 * User: scott
 * Date: 15/03/2018
 * Time: 13:23
 */

include "connect.php";

if (isset($_POST['insert'])) {

    $route_id = $_POST['route_id'];
    $data = array();

    $stmt = $conn->prepare("SELECT `POINT_ID` FROM `POINT` WHERE `ROUTE_ID` = ? ORDER BY `POINT_ID` ASC LIMIT 1");
    $stmt->bind_param("i", $route_id);
    $stmt->execute();

    $stmt->bind_result($min);
    $stmt->fetch();
    $stmt->close();

    $stmt = $conn->prepare("SELECT `POINT_ID` FROM `POINT` WHERE `ROUTE_ID` = ? ORDER BY `POINT_ID` DESC LIMIT 1");
    $stmt->bind_param("i", $route_id);
    $stmt->execute();

    $stmt->bind_result($max);
    $stmt->fetch();
    $stmt->close();


    $stmt = $conn->prepare("SELECT `ACCEL_Z` FROM `READING` WHERE `POINT_ID` >= ? AND `POINT_ID` <= ? ORDER BY `READING_ID`");
    $stmt->bind_param("ii", $min,$max);
    $stmt->execute();


    $result = mysqli_stmt_get_result($stmt);

    while($row = mysqli_fetch_array($result)) {
        $data[] = $row;
    }


    echo json_encode($data);
    $stmt->close();


}