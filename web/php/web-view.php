<?php
include "connect.php";

?>


<!DOCTYPE html>
<html lang="en">


<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="../images/mountain-black.png" type="image/x-icon"/>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
      integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
      integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
-->
<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"
        integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw=="
        crossorigin=""></script>
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css"
      integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ=="
      crossorigin=""/>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>
<script src="../js/web-ajax.js"></script>

<script src="../../lib/Leaflet.MultiOptionsPolyline/Leaflet.MultiOptionsPolyline.js"></script>


<title>Beinn.Bike</title>


<style>
    html, body {
        height: 100%;
        width: 100%;
        margin: 0;
    }

    #map {
        height: 70%;
        width: 100%;
    }

    h1 {
        display: inline;
    }

    .modal {
        display: none;
        position: fixed;
        z-index: 1000;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        background: rgba(255, 255, 255, .8) url('http://i.stack.imgur.com/FhHRx.gif') 50% 50% no-repeat;
    }

    /* When the body has the loading class, we turn
       the scrollbar off with overflow:hidden */
    body.loading {
        overflow: hidden;
    }

    /* Anytime the body has the loading class, our
       modal element will be visible */
    body.loading .modal {
        display: block;
    }

    .placeholder {
        display: none;
    }

    #chart-container {
        height: 60rem;
        width: 100%;
    }

    .chart {
        height: 30rem;
    }

    #meta {
        margin-left: 2rem;
    }

    .meta {
        margin-right: 1rem;
    }

    .info {
        padding: 6px 8px;
        font: 14px/16px Arial, Helvetica, sans-serif;
        background: white;
        background: rgba(255, 255, 255, 0.8);
        box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);
        border-radius: 5px;
    }

    .info h4 {
        margin: 0 0 5px;
        color: #777;
    }

    /*.legend {*/
    /*line-height: 18px;*/
    /*color: #555;*/
    /*}*/
    .legend i {
        width: 1.125rem;
        height: 1.125rem;
        float: left;
        margin-right: 0.5rem;
        opacity: 0.7;
    }


</style>
</head>
<body


<div class="container">
    <div class="page-header">
        <h1>Beinn.Bike</h1>

        <span id="meta">
        Route time:
        <span class="meta" id="route_time"></span>

        Average Speed:
        <span class="meta" id="avg_speed"></span>


        Distance:
        <span class="meta" id="distance"></span>
        </span>


    </div>
</div>

<div id="dropdown">

    <?php

    $sql = "SELECT ROUTE_ID,ROUTE_NAME,START_TIME FROM `ROUTE` WHERE ROUTE_NAME != ''";
    $result = $conn->query($sql);

    echo "<select class=\"form-control\" id = \"selectroute\" data-live-search=\"true\">";
    echo "<option class=\"placeholder\" selected disabled value=\"\">Select a route</option>";
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            echo "<option value =\"" . $row["ROUTE_ID"] . "\" data-subtext =\"" . $row["START_TIME"] . "\">" . $row["ROUTE_NAME"] . "</option>";
        }
    }

    echo "</select>";
    echo "<br>";
    ?>
</div>

<div id="map">


</div>


<div class="modal"><!-- Place at bottom of page --></div>

<script>

    /********MAP VARIABLES***********/

    var map;
    var route;
    var polyline = null;
    var startMark = null;
    var endMark = null;

    /*******************************/


    /******Chart variables*********/
    var altitudeArray = [];
    var altArray = [];
    var distanceArray = [];

    var accelChart = null;
    var altChart = null;

    /******************************/



    /***********MAP FUNCTIONS******************/
    /**
     *
     * Initialises the map and its neccesery components
     *
     * This includes creating the layer variables,
     * creating the map and adding the layer and legend
     * controls.
     *
     *
     * */
    function initMap() {

        var outdoor = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/outdoors-v9/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZG9uc2Nvc2hhIiwiYSI6ImNqZDF0OTBsdzBkcDkzM29xb2Zhc3o3ODMifQ.3-x6nnTgePEK0ERPllV7oQ',
            {
                id: 'one',
            });
        var satellite = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/satellite-streets-v9/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZG9uc2Nvc2hhIiwiYSI6ImNqZDFxc2FweDFna2cydnA0YmZhdXdqem0ifQ.5eLGg9F9PYXB9I4Q7ZrezA',
            {
                id: 'two'
            });

        map = L.map('map', {attributionControl: false, layers: [outdoor, satellite]}).fitWorld();

        var baseMaps = {
            "Satellite": satellite,
            "Outdoor": outdoor
        };

        L.control.layers(baseMaps).addTo(map);
        createLegend(map);
    }

    function createLegend(map) {
        var legend = L.control({position: 'bottomleft'});

        //create a legend and position at bottom left of map
        legend.onAdd = function (map) {

            var legendDiv = L.DomUtil.create('div', 'info legend');

            legendDiv.innerHTML += '<table style="border-collapse: separate; -webkit-border-horizontal-spacing: 1rem; ' +
                '-webkit-border-vertical-spacing: 0.2rem">' +
                '<tr><td><i style = "background:#3dff11"></td><td>Smooth</td>' +
                '<td><i style = "background:#FFC000"></td><td>Rough</td></tr>' +
                '<tr><td><i style = "background:#FF0000"></i></td><td>Very Rough</td>' +
                '<td><i style = "background:#ff17bc"></i></td><td>Event</td></tr>';

            return legendDiv;
        };

        legend.addTo(map);
    }


    /**
     * Draw a route and colour code it based on
     * the routes roughness
     **/
    function drawRouteColour() {
        var latlngs = [];
        var points = route.getPoints();

        //Remove old route
        if (polyline != null) {
            map.removeLayer(polyline);
        }

        //create JSON
        for (var i = 0; i < points.length; i++) {
            var p = points[i];
            var temp = L.latLng(p.getLat(), p.getLng(), p.getAlt());
            temp.meta = {terrain: p.getTerrain()};
            latlngs.push(temp);
        }

        console.log(latlngs);

        var visibleTrack = L.featureGroup();
        polyline = L.multiOptionsPolyline(latlngs, {
            multiOptions: {
                optionIdxFn: function (latLng) {
                    var i, terrain = latLng.meta.terrain,
                        choice = ["SMOOTH", "ROUGH", "VERY_ROUGH", "EVENT"];
                    for (i = 0; i < choice.length; ++i) {
                        if (terrain == choice[i]) {
                            return i;
                        }
                    }
                    return choice.length;
                },
                options: [
                    {color: '#3dff11'}, {color: '#FFC000'}, {color: '#FF0000'}, {color: '#ff17bc'}
                ]
            },
            weight: 5,
            lineCap: 'butt',
            opacity: 0.75,
            smoothFactor: 1
        }).addTo(visibleTrack);

        map.fitBounds(polyline.getBounds());

        visibleTrack.addTo(map);

        var green = L.icon({
            iconUrl: '../images/finish-circle-green.png',
            iconSize: [25, 25],
        });

        var red = L.icon({
            iconUrl: '../images/finish-circle-red.png',
            iconSize: [25, 25],
        });

        if (startMark != null) {
            map.removeLayer(startMark);
        }if (endMark != null) {
            map.removeLayer(endMark);
        }

        startMark = L.marker([points[0].getLat(), points[0].getLng()], {icon: green}).addTo(map);
        endMark = L.marker([points[points.length - 1].getLat(), points[points.length - 1].getLng()], {icon: red}).addTo(map);

    }

    /**
     *
     * hello
     *
     * @return integer
     *
     * */
    function parseRoute(json) {
        return new Promise(function (fulfill, reject) {
            altitudeArray = [];
            altArray = [];
            distanceArray = [];
            //reject(error); //if the action did not succeed
            //console.log("Parse route" + json);
            var obj = JSON.parse(json);
            console.log(obj);
            for (var key in obj) {
                if (obj.hasOwnProperty(key)) {
                    var id = obj[key].POINT_ID;
                    var lat = obj[key].LAT;
                    var lng = obj[key].LNG;
                    var alt = obj[key].ALT;
                    var time = obj[key].TIME;
                    var distance = obj[key].POINT_DISTANCE;
                    var terrain = obj[key].TERRAIN;
                    //console.log("TERRAIN: " + terrain);
                    altitudeArray.push({x: alt, y: distance});

                    altArray.push(alt);
                    //if (distanceArray[distanceArray.length - 1] !== distance) {
                    distanceArray.push(distance);
                    //}
                    route.addPoint(new Point(id, lat, lng, alt, time, distance, terrain));
                }
            }


            fulfill(); //if the action succeeded

        });

    }

    function setSpans(json) {
        console.log("IM IN HERE");
        var obj = JSON.parse(json);

        var route_time = obj[0].TIME;
        var avg_speed = obj[0].AVG_SPEED;
        var distance = obj[0].DISTANCE;

        $("#route_time").html(route_time);
        $("#avg_speed").html(avg_speed);
        $("#distance").html(distance);

        console.log("got the spans:" + route_time + " " + avg_speed + " " + distance);
    }

    $(document).ready(function () {

        $('#selectroute').on('change', function () {
            console.log("hello");
            var route_id = $("#selectroute").val();
            console.log(route_id);
            console.log($("#selectroute option:selected").text());

            route = new Route(route_id, null);

            requestRoute(route_id, function (json) {
                setSpans(json);
            });
            requestPoints(route_id, function (json) {
                parseRoute(json).then(function () {
                    drawRouteColour();
                    createAltitudeChart();
                    requestAccel(route_id, createAccelChart);
                })
            });
        });


        //Create the chart divs only once
        //Removes large empty whitespace on initial load
        $('#selectroute').one('change', function () {
            $('<div id = "chart-container">\n' +
                '<div class="chart">\n' +
                '    <canvas id="accel-chart"></canvas>\n' +
                '</div>\n' +
                '<div class ="chart">\n' +
                '<canvas id="alt-chart"></canvas>\n' +
                '</div>\n' +
                '</div>').appendTo('body');
        });


        initMap();
    });

    $(document).on({
        ajaxStart: function () {
            $("body").addClass("loading");
        },
        ajaxStop: function () {
            $("body").removeClass("loading");
        }
    });


    /*********************CHARTING FUNCTIONS**************************************/


    function createAccelChart(json) {
        var accelArray = [];
        var labels = [];
        var obj = JSON.parse(json);

        var i = 0;
        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                var accel_z = obj[key].ACCEL_Z;
                accelArray.push(accel_z);
                labels.push('');
                i++;
            }
        }

        var canvas = $('#accel-chart');
        var context = canvas.get(0).getContext('2d');

        if (accelChart != null) {
            accelChart.destroy();
        }

        var options = {
            title: {
                display: true,
                text: '',
                pointRadius: 0,
                animation: false
            },
            elements: {
                point: {
                    radius: 0
                }
            },
            scales: {
                yAxes: [{
                    display: true,
                    ticks: {
                        max: 16,
                        min: -16,
                        stepSize: 4
                    },
                }],
                xAxes: [{
                    gridLines: {
                        color: "rgba(0, 0, 0, 0)"
                    }
                }]
            },
            responsive: true,
            maintainAspectRatio: false
        };


        accelChart = new Chart(context, {
            type: 'line',
            data: {
                labels: labels,
                datasets: [{
                    data: accelArray,
                    label: "Accelerometer Z",
                    borderColor: "#cd8721",
                    fill: false
                }]
            },
            options: options
        });


    }

    function createAltitudeChart() {

        var options = {
            title: {
                display: true,
                text: '',
                animation: false
            },
            scales: {
                xAxes: [{
                    ticks: {
                        stepSize: 0.5,
                        gridLines: {
                            color: "rgba(0, 0, 0, 0)"
                        }
                    }
                }],
                yAxes: [{
                    ticks: {}
                }]
            },
            elements: {
                point: {
                    radius: 0
                }
            },
            responsive: true,
            maintainAspectRatio: false
        };


        console.log(distanceArray);
        console.log(altArray);

        var canvas = $('#alt-chart');
        var context = canvas.get(0).getContext('2d');

        if (altChart != null) {
            altChart.destroy();
        }

        altChart = new Chart(context, {
            type: 'line',
            data: {
                labels: distanceArray,
                datasets: [{
                    data: altArray,
                    label: "Altitude",
                    borderColor: "#3e95cd",
                    fill: true,
                    backgroundColor: "#6db8cd",
                    backgroundOpacity: 0.9

                }]
            },
            options: options
        });
        console.log(distanceArray.length);
        console.log(altArray.length);

    }


</script>


</body>
</html>
